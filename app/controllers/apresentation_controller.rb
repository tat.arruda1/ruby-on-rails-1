class ApresentationController < ApplicationController
    
    def home
        
    end

    def buttons
        @number1 = params[:number1].to_i
        @number2 = params[:number2].to_i
        @add = @number1 + @number2
        @sub = @number1 - @number2
        @mult = @number1 * @number2
        @div = @number1 / @number2
    end

    def adicao

        @add = params[:result].to_i

    end

    def multiplicacao

        @mult = params[:result].to_i
        
    end

    def subtracao
        @sub = params[:result].to_i
    end

    def divisao
        @div = params[:result].to_i
    end

end