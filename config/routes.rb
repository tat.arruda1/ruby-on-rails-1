Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/calculator', to: 'apresentation#home'
  get '/calculator/:number1/:number2', to: 'apresentation#buttons'
  get '/subtracao/:result', to: "apresentation#subtracao", as: "sub"
  get '/divisao/:result', to: "apresentation#divisao", as: "div"
  get '/multiplicacao/:result', to: "apresentation#multiplicacao", as: "mult"
  get '/adicao/:result', to: "apresentation#adicao", as: "add"
end
